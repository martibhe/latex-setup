\documentclass[a4paper, UKenglish]{article}


%% Font
\usepackage[utf8]{inputenc}    % \usepackage[latin1]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage{lmodern}           % Latin Modern Roman 
\usepackage[scaled]{beramono}  % Bera Mono (Bitstream Vera Sans Mono)
\renewcommand{\sfdefault}{phv} % Helvetica
\usepackage{sectsty}
\allsectionsfont{\sffamily}
\usepackage{microtype}         % Improved typography


%% Mathematics
\usepackage{amssymb}    % Extra symbols
\usepackage{amsthm}     % Theorem-like environments 
\usepackage{thmtools}   % Theorem-like environments
\usepackage{mathtools}  % Fonts and environments for mathematical formuale 
\usepackage{mathrsfs}   % Script font with \mathscr{}


%% Miscellanous
\usepackage{graphicx}   % Tool for images
\usepackage{subcaption} % Tool for images
\usepackage{babel}      % Automatic translations
\usepackage{csquotes}   % Quotes
\usepackage{textcomp}   % Extra symbols
\usepackage{booktabs}   % Tool for tables
\usepackage{listings}   % Typesetting code
\lstset{basicstyle = \ttfamily, frame = single}
\usepackage{url}
\urlstyle{sf}


%% Bibliography
\usepackage[backend = biber, style = alphabetic]{biblatex}
%\addbibresource{<NAME OF BIBLIOGRAPY FILE>.bib}


%% Cross references
\usepackage{varioref}
\usepackage[hypertexnames = false]{hyperref}
\usepackage[nameinlink, capitalize, noabbrev]{cleveref}
\usepackage{autonum}
\makeatletter
\autonum@generatePatchedReference{vref}
\makeatother


%% Teorem-like environments
\declaretheorem[style = plain, numberwithin = section]{theorem}
\declaretheorem[style = plain,      sibling = theorem]{corollary}
\declaretheorem[style = plain,      sibling = theorem]{lemma}
\declaretheorem[style = plain,      sibling = theorem]{proposition}
\declaretheorem[style = definition, sibling = theorem]{definition}
\declaretheorem[style = definition, sibling = theorem]{example}
\declaretheorem[style = remark,    numbered = no]{remark}
\declaretheorem[style = definition]{exercise}
\declaretheorem[style = remark, numbered = no, qed = $\blacksquare$]{solution}


%% Delemiters
\DeclarePairedDelimiter{\p}{\lparen}{\rparen}  % Parenthesis
\DeclarePairedDelimiter{\set}{\{}{\}}          % Set
\DeclarePairedDelimiter{\abs}{|}{|}            % Absolute value
\DeclarePairedDelimiter{\norm}{\|}{\|}         % Norm


%% Operators
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\E}{E}
\DeclareMathOperator{\Var}{Var}
\DeclareMathOperator{\Cov}{Cov}


%% New commands for sets
\newcommand{\N}{\mathbb{N}}    % Natural numbers
\newcommand{\Z}{\mathbb{Z}}    % Integers
\newcommand{\Q}{\mathbb{Q}}    % Rational numbers
\newcommand{\R}{\mathbb{R}}    % Real numbers
\newcommand{\C}{\mathbb{C}}    % Complex numbers
\newcommand{\A}{\mathbb{A}}    % Affine space
\renewcommand{\P}{\mathbb{P}}  % Projective space


%% New commands for vectors
\renewcommand{\a}{\mathbf{a}}
\renewcommand{\b}{\mathbf{b}}
\renewcommand{\c}{\mathbf{c}}
\renewcommand{\v}{\mathbf{v}}
\newcommand{\x}{\mathbf{x}}
\newcommand{\y}{\mathbf{y}}
\newcommand{\0}{\mathbf{0}}
\newcommand{\1}{\mathbf{1}}


\author{\textsc{Martin Helsø}}
\title{\sffamily A short introduction to \LaTeX}


\begin{document}


\maketitle


\section{Mathematics}
This section gives various examples of typesetting mathematics. There is no coherence between the subjects. It is recommended to switch between reading the compiled document and the source code.

\begin{exercise}
    Find the solutions of the quadratic equation \( x^2 - 4x + 3 = 0 \).
\end{exercise}

\begin{solution}
    The roots of the polynomial \( ax^2 + bx + c \) are described by the quadratic formula
    \begin{align}
        x = \frac{ -b \pm \sqrt{b^2 - 4ac} } {2a}.
    \end{align}
    Insertion yields
    \begin{align}
        x = \frac{ 4 \pm \sqrt{4^2 - 4 \cdot 1 \cdot 3} }{2 \cdot 1} = 2 \pm 1,
    \end{align}
    thus the solutions are \( x_1 = 1 \) and \( x_2 = 3 \).
\end{solution}

\begin{example}
    The quadratic form of a symmetric \( 3 \times 3 \) matrix \( A \) is a square over \( \C \) if and only if \( \rank A = 1 \). The quadratic form is particularly nice if 
    \begin{align}
        A
        \coloneqq
        \begin{bmatrix}
            1 & a   & b  \\
            a & a^2 & ab \\
            b & ab  & b^2
       \end{bmatrix}.
    \end{align}
    Then the quadratic form is
    \begin{align}
        \x^T A \x
        &=
        \begin{bmatrix}
            x & y & z
        \end{bmatrix}
        \begin{bmatrix}
            1 & a   & b  \\
            a & a^2 & ab \\
            b & ab  & b^2
        \end{bmatrix}
        \begin{bmatrix}
            x \\ y \\ z
        \end{bmatrix}
        \\
        &= 
        \begin{bmatrix}
            x & y & z
        \end{bmatrix}
        \begin{bmatrix}
             x + ay   + bz  \\
            ax + a^2y + abz \\
            bx + aby  + b^2z
        \end{bmatrix}
        \\
        &=
        x(x + ay + bz) + y(ax + a^2y + abz) + z(bx + aby + b^2z)    
        \\
        &= 
        x^2 + 2axy + a^2y^2 + 2bxz + 2abyz + b^2z^2
        \\
        &=
        (x + az + by)^2.
    \end{align}       
\end{example}

\begin{exercise}
    Let \( f \colon \R \to \R \) be given by
    \begin{align}
        f(x) 
        \coloneqq 
        \begin{dcases}
            \ln(x) & x \ge 1, \\
            6x + 3 & x < 1.
        \end{dcases}
    \end{align}
    Determine whether \( f \) is continuous.
\end{exercise}

\begin{solution}
    The function \( f \) is discontinuous because the one-sided limit
    \begin{align}
        \lim_{x \to 1^-} f(x) = \lim_{x \to 1^-} 6x + 3 = 9 
    \end{align}
    differs from \( f(1) = 0 \).
\end{solution}

\begin{exercise}
    Find the area bounded by the graph of \( \sin x \) and the lines \( x = 0\) and \( x = \pi \).
\end{exercise}

\begin{solution}
    We calculate the definite integral
    \begin{align}
        \int_{0}^{\pi} \sin x \, \mathrm{d} x 
        = 
        [ -\cos x]_{0}^{\pi} 
        = 
        1 + 1 
        = 
        2.
    \end{align}
    Hence the area is 2.
\end{solution}

\begin{theorem}
    \label{thm}
    This is the first theorem in this document.
\end{theorem}

\begin{proof}
    Trivial.
\end{proof}


\subsection{Delimiters with Adjustable Height}
Often one need to adjust the height of delimiters such as (\ldots) and \{\ldots\} to the height of their content. This can be done by inserting the commands \texttt{\textbackslash left} and \texttt{\textbackslash right} in front of the left and right delimiter, respectively. In addition, \texttt{\textbackslash middle} can be used on some symbols to give the content the same height as the delimiters.

However, we defined the commands \texttt{\textbackslash p}, \texttt{\textbackslash set}, \texttt{\textbackslash abs} and \texttt{\textbackslash norm}, and you can define more as needed. By default,  \texttt{\textbackslash p\{\ldots\}} will encapsulate its argument in ordinary parentheses, but if the starred version \texttt{\textbackslash p*\{\ldots\}} is used, then the argument with be encapsulated by parentheses of adjustable height. 

Compare for instance\footnote{It is more natural to use \texttt{\textbackslash mid}, but it does not scale, hence \texttt{\textbackslash arrowvert} in the second version.}
\begin{align}
    \set{ (x_1, \ldots, x_n) \in \R^n \mid \sum_{i = 1}^{n} \p{ \frac{x_i}{i} }^2 = 1 },
\intertext{and}
    \set*{ (x_1, \ldots, x_n) \in \R^n \middle \arrowvert \sum_{i = 1}^{n} \p*{ \frac{x_i}{i} }^2 = 1 }.
\end{align}


\section{Cross References}
To refer to an equation, theorem, table, image or section, give the object a key with \texttt{\textbackslash label\{\emph{key}\}}. Then you can refer to it with one of the commands in \vref{tab:crossreferences}, for example \texttt{\textbackslash ref\{\emph{key\}}}.

The command \texttt{\textbackslash eqref} is intended for equations, but both \texttt{\textbackslash cref} and \texttt{\textbackslash vref} will add parentheses if they refer to an equation.

\begin{table}
\centering
\begin{tabular}{ccc}
    \toprule
    Command & Description & Example \\
    \midrule 
    \texttt{\textbackslash ref}     & Number                 & \ref{thm}     \\
    \texttt{\textbackslash eqref}   & Number in parentheses  & \eqref{thm}   \\
    \texttt{\textbackslash pageref} & Page number            & \pageref{thm} \\
    \texttt{\textbackslash cref}    & Type and number        & \cref{thm}    \\
    \texttt{\textbackslash vref} & Type, location and number & \vref{thm}    \\
    \bottomrule
\end{tabular}
\caption{Cross reference commands.}
\label{tab:crossreferences}
\end{table}

The package \texttt{autonum} ensures that equations are unnumbered until they have been referred to\footnote{This does not apply to \texttt{\textbackslash pageref}, as it only refers to the page and not the equation itself.}. In addition, the package removes the starred versions of equations, so  \texttt{align*} and \texttt{equation*} will result in compilation errors. If this effect is unwanted, remove out the following lines from the preamble:

\begin{lstlisting}[language     = {[LaTeX]TeX}, 
                   morekeywords = {\autonum@generatePatchedReference}]
\usepackage{autonum}
\makeatletter
\autonum@generatePatchedReference{vref}
\makeatother
\end{lstlisting}
Remove the comments and test if you can still compile this document.

When it comes to cross references, it is important which order they are imported in. In general, they should be the last packages to be imported, and to get the correct interplay between the different cross reference packages, they have to be imported in a specific order. Be careful of this when you expand this template.


\end{document}